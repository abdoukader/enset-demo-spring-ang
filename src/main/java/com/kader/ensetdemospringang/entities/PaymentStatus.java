package com.kader.ensetdemospringang.entities;

public enum PaymentStatus {
    CREATED, VALIDATED, REJECTED
}
