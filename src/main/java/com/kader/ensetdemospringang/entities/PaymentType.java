package com.kader.ensetdemospringang.entities;

public enum PaymentType {
    CASH, CHECK, TRANSFERT, DEPOSIT
}
